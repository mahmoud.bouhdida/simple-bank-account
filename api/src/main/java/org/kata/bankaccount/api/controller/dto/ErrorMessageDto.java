package org.kata.bankaccount.api.controller.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.kata.bankaccount.api.util.DateTimeUtil;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
public class ErrorMessageDto {
    private HttpStatus status;

    private String timestamp;

    private String message;

    @Builder
    public ErrorMessageDto(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
        this.timestamp = DateTimeUtil.formatDateFr(LocalDateTime.now());
    }
}
