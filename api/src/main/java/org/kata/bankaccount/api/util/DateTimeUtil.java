package org.kata.bankaccount.api.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class DateTimeUtil {

    /**
     * Formats a date using French locale with current timezone
     *
     * @param date Date object
     * @return String representation of the date in French locate
     */
    public static String formatDateFr(LocalDateTime date) {
        Locale frLocale = new Locale("fr", "FR");
        DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                .withLocale(frLocale)
                .withChronology(Chronology.ofLocale(frLocale));

        ZonedDateTime zonedDate = date.atZone(ZoneId.systemDefault());

        return dateTimeFormat.format(zonedDate);
    }
}
