package org.kata.bankaccount.persistence;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.kata.bankaccount.persistence.entity.AccountEntity;
import org.kata.bankaccount.persistence.entity.TransactionEntity;
import org.kata.bankaccount.persistence.repository.AccountRepository;
import org.kata.bankaccount.persistence.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class TransactionRepositoryTests {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;
    private String owner;
    private String phoneNumber;
    private AccountEntity accountEntity;

    @BeforeEach
    public void init() {
        owner = "John Smith";
        phoneNumber = "+33612345678";
        accountEntity = AccountEntity.builder()
                .owner(owner)
                .phoneNumber(phoneNumber)
                .build();

        TransactionEntity transactionEntity1 = TransactionEntity.builder()
                .amount(500)
                .balance(500)
                .date(LocalDateTime.now()).type(TransactionEntity.DEPOSIT_OPERATION_INT)
                .account(accountEntity)
                .build();

        TransactionEntity transactionEntity2 = TransactionEntity.builder()
                .amount(100)
                .balance(400)
                .date(LocalDateTime.now()).type(TransactionEntity.WITHDRAWAL_OPERATION_INT)
                .account(accountEntity)
                .build();

        TransactionEntity transactionEntity3 = TransactionEntity.builder()
                .amount(200)
                .balance(600)
                .date(LocalDateTime.now()).type(TransactionEntity.DEPOSIT_OPERATION_INT)
                .account(accountEntity)
                .build();

        accountRepository.save(accountEntity);
        Arrays.asList(transactionEntity1, transactionEntity2, transactionEntity3).forEach(tr -> transactionRepository.save(tr));
    }

    @DisplayName("When I have transactions in the DB, the returned list should have the same size")
    @Test
    public void should_returnListWithCorrectSize_when_transactionsAvailableInDB() {
        List<TransactionEntity> transactionEntities = transactionRepository.findAll();
        assertEquals(3, transactionEntities.size(), "Transaction list size is incorrect");
    }

    @DisplayName("When I have transactions in the DB and I add a new one, the returned list should have a correct size")
    @Test
    public void should_returnListWithCorrectSize_when_newTransactionIsAddedToDB() {
        int oldListSize = transactionRepository.findAll().size();

        TransactionEntity transactionEntity = TransactionEntity.builder()
                .amount(100)
                .balance(600)
                .date(LocalDateTime.now()).type(TransactionEntity.DEPOSIT_OPERATION_INT)
                .account(accountEntity)
                .build();
        transactionRepository.save(transactionEntity);
        List<TransactionEntity> transactionEntities = transactionRepository.findAll();
        assertEquals(oldListSize + 1, transactionEntities.size(), "Transaction list size is incorrect");
    }

    @DisplayName("When I create a new account in the DB, the returned account list should have the correct size")
    @Test
    public void should_returnCorrectSize_when_accountCreated() {
        List<AccountEntity> accountEntities = accountRepository.findAll();
        assertEquals(1, accountEntities.size(), "Account list size is incorrect");
    }

    @DisplayName("When I create a new account in the DB, the returned account entity should have non empty infos")
    @Test
    public void should_returnAccountInfo_when_accountCreated() {
        AccountEntity accountEntity = accountRepository.findAll().get(0);
        assertEquals(owner, accountEntity.getOwner(), "Account owner's name mismatching");
        assertEquals(phoneNumber, accountEntity.getPhoneNumber(), "Account owner's phone number mismatching");
    }
}
