package org.kata.bankaccount.persistence.adapter;

import org.kata.bankaccount.domain.model.Operation;
import org.kata.bankaccount.domain.port.spi.BankAccPersistencePort;
import org.kata.bankaccount.persistence.entity.AccountEntity;
import org.kata.bankaccount.persistence.entity.TransactionEntity;
import org.kata.bankaccount.persistence.repository.AccountRepository;
import org.kata.bankaccount.persistence.repository.TransactionRepository;
import org.kata.bankaccount.persistence.util.EntityConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BankAccPersistenceImpl implements BankAccPersistencePort {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<Operation> loadOperations() {
        return transactionRepository.findAll(Sort.by(Sort.Direction.DESC, "date")).stream().map(EntityConverter::toOperationModel).toList();
    }

    @Override
    public Operation saveOperation(Operation operation) {
        TransactionEntity transactionEntity = EntityConverter.fromOperationModel(operation);
        transactionEntity.setAccount(getUniqueAccount());
        TransactionEntity savedEntity = transactionRepository.save(transactionEntity);
        return EntityConverter.toOperationModel(savedEntity);
    }

    @Override
    public int createNewAccount(String owner, String phoneNumber) {
        AccountEntity account = AccountEntity.builder()
                .owner(owner)
                .phoneNumber(phoneNumber)
                .build();
        return accountRepository.save(account).getId();
    }

    private AccountEntity getUniqueAccount() {
        List<AccountEntity> accountEntities = accountRepository.findAll();
        if (!accountEntities.isEmpty())
            return accountEntities.get(0);
        return null;
    }

    @Override
    public String getOwnerName(int accountNumber) {
        Optional<AccountEntity> queryResult = accountRepository.findById(accountNumber);
        if (queryResult.isPresent())
            return queryResult.get().getOwner();
        return "";
    }

    @Override
    public String getOwnerPhoneNumber(int accountNumber) {
        Optional<AccountEntity> queryResult = accountRepository.findById(accountNumber);
        if (queryResult.isPresent())
            return queryResult.get().getPhoneNumber();
        return "";
    }
}
