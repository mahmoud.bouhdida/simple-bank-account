package org.kata.bankaccount.persistence.repository;

import org.kata.bankaccount.persistence.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {
}
