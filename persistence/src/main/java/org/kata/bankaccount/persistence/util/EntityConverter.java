package org.kata.bankaccount.persistence.util;

import org.kata.bankaccount.domain.model.Operation;
import org.kata.bankaccount.persistence.entity.TransactionEntity;

public class EntityConverter {

    /**
     * Converts an Operation model to a Transaction Entity
     *
     * @param operation {@link Operation} model
     * @return An {@link TransactionEntity} object
     */
    public static TransactionEntity fromOperationModel(Operation operation) {
        return TransactionEntity.builder()
                .balance(operation.getBalance())
                .type(operation.getType() == Operation.Type.DEPOSIT ?
                        TransactionEntity.DEPOSIT_OPERATION_INT : TransactionEntity.WITHDRAWAL_OPERATION_INT)
                .amount(operation.getAmount())
                .date(operation.getDate())
                .build();
    }

    /**
     * Converts a Transaction Entity to an Operation model
     *
     * @param transactionEntity {@link TransactionEntity} object
     * @return An {@link Operation} model
     */
    public static Operation toOperationModel(TransactionEntity transactionEntity) {
        Operation operation = Operation.builder()
                .type(transactionEntity.getType() == TransactionEntity.DEPOSIT_OPERATION_INT ?
                        Operation.Type.DEPOSIT : Operation.Type.WITHDRAW)
                .amount(transactionEntity.getAmount())
                .build();
        operation.setBalance(transactionEntity.getBalance());
        operation.setDate(transactionEntity.getDate());

        return operation;
    }
}
