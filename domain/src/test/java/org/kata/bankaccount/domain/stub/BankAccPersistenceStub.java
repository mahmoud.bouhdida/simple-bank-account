package org.kata.bankaccount.domain.stub;

import org.kata.bankaccount.domain.model.Operation;
import org.kata.bankaccount.domain.port.spi.BankAccPersistencePort;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BankAccPersistenceStub implements BankAccPersistencePort {

    private final List<Operation> operationList;
    private String name;
    private String phoneNumber;

    public BankAccPersistenceStub() {
        this.operationList = new ArrayList<>();
    }

    @Override
    public List<Operation> loadOperations() {
        return new ArrayList<>(operationList);
    }

    @Override
    public Operation saveOperation(Operation operation) {
        operationList.add(operation);
        return operation;
    }

    @Override
    public int createNewAccount(String owner, String phoneNumber) {
        this.name = owner;
        this.phoneNumber = phoneNumber;
        return new Random().nextInt(Integer.MAX_VALUE);
    }

    @Override
    public String getOwnerName(int accountNumber) {
        return name;
    }

    @Override
    public String getOwnerPhoneNumber(int accountNumber) {
        return phoneNumber;
    }
}
