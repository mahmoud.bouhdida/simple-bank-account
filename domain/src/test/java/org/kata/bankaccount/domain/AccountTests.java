package org.kata.bankaccount.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AccountTests extends BaseTests {

    @DisplayName("When I request the account number, the returned number shouldn't be 0")
    @Test
    public void should_accountNumberNotZero_when_getAccountNumber() {
        assertNotEquals(0, bankAccount.getNumber(), "Account number is incorrect");
    }

    @DisplayName("When I request the account owner name, the returned name shouldn't be empty")
    @Test
    public void should_accountOwnerNameNonEmpty_when_getOwnerName() {
        assertNotNull(bankAccount.getOwnerName(), "Account owner name is null");
        assertFalse(bankAccount.getOwnerName().isEmpty(), "Account owner name is incorrect");
    }

    @DisplayName("When I request the account owner phone number, the returned number shouldn't be empty")
    @Test
    public void should_accountOwnerPhoneNumberNonEmpty_when_getOwnerPhoneNumber() {
        assertNotNull(bankAccount.getOwnerPhoneNumber(), "Account owner phone number is null");
        assertFalse(bankAccount.getOwnerPhoneNumber().isEmpty(), "Account owner phone number is incorrect");
    }
}
