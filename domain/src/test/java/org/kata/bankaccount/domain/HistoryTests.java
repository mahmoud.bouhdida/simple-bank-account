package org.kata.bankaccount.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.kata.bankaccount.domain.exception.InsufficientBalanceException;
import org.kata.bankaccount.domain.model.Operation;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HistoryTests extends BaseTests {

    @DisplayName("When I do some operations on my account, the history size follows")
    @Test
    public void should_historySizeIncrease_when_addAccountOperations() {
        int firstDeposit = 450;
        int withdrawal = 200;
        int secondDeposit = 250;

        bankAccount.deposit(firstDeposit);
        bankAccount.withdraw(withdrawal);
        bankAccount.deposit(secondDeposit);

        assertEquals(3, bankAccount.getHistory().getOperationList().size(),
                "The number of account operations is incorrect");
    }

    @DisplayName("When I withdraw from my account and I have a sufficient balance, it will be the last operation")
    @Test
    public void should_withdrawBeLastOperation_when_withdrawFromAccountSufficientBalance() {
        int initialDeposit = 650;
        int withdrawalAmount = 200;
        LocalDateTime startDate = LocalDateTime.now();

        bankAccount.deposit(initialDeposit);
        bankAccount.withdraw(withdrawalAmount);

        int expectedBalance = initialDeposit - withdrawalAmount;
        LocalDateTime endDate = LocalDateTime.now();

        Operation lastOperation = bankAccount.getHistory().getLatestOperation();
        assertAll("The last operation details needs to be the same as the withdrawal",
                () -> assertEquals(lastOperation.getAmount(), -1 * withdrawalAmount,
                        "The last operation amount is incorrect"),
                () -> assertEquals(lastOperation.getType(), Operation.Type.WITHDRAW,
                        "The last operation type is incorrect"),
                () -> assertEquals(expectedBalance, lastOperation.getBalance(),
                        "The last operation balance is incorrect"),
                () -> assertTrue(lastOperation.getDate().equals(startDate)
                                || lastOperation.getDate().equals(endDate)
                                || (lastOperation.getDate().isAfter(startDate)
                                && lastOperation.getDate().isBefore(endDate)),
                        () -> String.format("The last operation date is incorrect. It should be between [%s and %s]. " +
                                "Actual value is: %s", startDate, endDate, lastOperation.getDate()))
        );
    }

    @DisplayName("When I withdraw from my account and I have an insufficient balance, no operation is added")
    @Test
    public void should_noOperationAdded_when_withdrawFromAccountInsufficientBalance() {
        int initialDeposit = 350;
        int withdrawalAmount = 700;

        bankAccount.deposit(initialDeposit);

        assertAll("There should be no added operation after the initial deposit",
                () -> assertThrows(InsufficientBalanceException.class,
                        () -> bankAccount.withdraw(withdrawalAmount),
                        "The withdrawal didn't raise an exception with an insufficient balance"),
                () -> assertEquals(1, bankAccount.getHistory().getOperationList().size(),
                        "History operations size is incorrect"));
    }

    @DisplayName("When I make a deposit in my account, it will be the last operation")
    @Test
    public void should_depositBeLastOperation_when_makeDeposit() {
        int initialDeposit = 740;
        int withdrawalAmount = 380;
        LocalDateTime startDate = LocalDateTime.now();

        bankAccount.deposit(initialDeposit);
        bankAccount.withdraw(withdrawalAmount);
        bankAccount.deposit(initialDeposit);

        int expectedBalance = 2 * initialDeposit - withdrawalAmount;
        LocalDateTime endDate = LocalDateTime.now();

        Operation lastOperation = bankAccount.getHistory().getLatestOperation();
        assertAll("The last operation details needs to be the same as the deposit",
                () -> assertEquals(lastOperation.getAmount(), initialDeposit,
                        "The last operation amount is incorrect"),
                () -> assertEquals(lastOperation.getType(), Operation.Type.DEPOSIT,
                        "The last operation type is incorrect"),
                () -> assertEquals(expectedBalance, lastOperation.getBalance(),
                        "The last operation balance is incorrect"),
                () -> assertTrue(lastOperation.getDate().equals(startDate)
                                || lastOperation.getDate().equals(endDate)
                                || (lastOperation.getDate().isAfter(startDate)
                                && lastOperation.getDate().isBefore(endDate)),
                        () -> String.format("The last operation date is incorrect. It should be between [%s and %s]. " +
                                "Actual value is: %s", startDate, endDate, lastOperation.getDate()))
        );
    }

    @DisplayName("When I make no operations in my account, the history is empty")
    @Test
    public void should_operationsHistoryEmpty_when_noOperationsInAccount() {
        assertAll("The operations history needs to be empty",
                () -> assertEquals(0, bankAccount.getHistory().getOperationList().size(),
                        "The operations history is not empty."),
                () -> assertNull(bankAccount.getHistory().getLatestOperation(),
                        "The last operation is not null")
        );
    }
}

