package org.kata.bankaccount.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 * The operation DTO
 */
@Getter
@Setter
public class Operation {

    private Type type;
    private int balance;
    private int amount;
    private LocalDateTime date;

    /**
     * Operation constructor
     *
     * @param type    Operation type
     * @param balance Account balance after operation
     * @param amount  Operation amount
     */
    @Builder
    public Operation(Type type, int balance, int amount) {
        this.type = type;
        this.balance = balance;
        this.amount = (type == Type.WITHDRAW && amount > 0) ? -1 * amount : amount;
        this.date = LocalDateTime.now();
    }

    /**
     * Operation type
     */
    public enum Type {
        /**
         * Withdraw operation
         */
        WITHDRAW,
        /**
         * Deposit operation
         */
        DEPOSIT
    }
}
