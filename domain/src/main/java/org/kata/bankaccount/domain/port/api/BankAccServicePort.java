package org.kata.bankaccount.domain.port.api;

import org.kata.bankaccount.domain.model.Account;

/**
 * Bank account service port
 */
public interface BankAccServicePort {

    /**
     * @return the bank account domain model
     */
    Account getBankAccount();
}
