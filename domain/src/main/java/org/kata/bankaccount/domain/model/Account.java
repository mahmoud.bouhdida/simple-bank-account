package org.kata.bankaccount.domain.model;

import net.datafaker.Faker;
import org.kata.bankaccount.domain.exception.InsufficientBalanceException;
import org.kata.bankaccount.domain.exception.InvalidOperationAmountException;
import org.kata.bankaccount.domain.port.spi.BankAccPersistencePort;

/**
 * The bank account model
 */
public class Account {

    private static final Faker faker = new Faker();

    private final History history;
    private final int number;

    private final BankAccPersistencePort bankAccPersistencePort;

    /**
     * Account constructor
     *
     * @param bankAccPersistencePort Bank account persistence port
     */
    public Account(BankAccPersistencePort bankAccPersistencePort) {
        String owner = faker.name().fullName();
        String phoneNumber = faker.phoneNumber().cellPhone();

        this.number = bankAccPersistencePort.createNewAccount(owner, phoneNumber);
        this.history = new History(bankAccPersistencePort);
        this.bankAccPersistencePort = bankAccPersistencePort;
    }


    /**
     * Make a deposit in the account
     *
     * @param depositAmount The amount to deposit
     * @return The deposit operation
     */
    public Operation deposit(int depositAmount) {
        if (depositAmount <= 0)
            throw new InvalidOperationAmountException
                    (Operation.Type.DEPOSIT.name(), depositAmount, "Deposit amount cannot be 0 or less");
        return history.addDeposit(depositAmount);
    }

    /**
     * Withdrawal from the account
     *
     * @param withdrawalAmount The amount to withdraw
     * @return The withdrawal operation
     * @throws InsufficientBalanceException Thrown when the account balance is insufficient
     */
    public Operation withdraw(int withdrawalAmount) {
        if (withdrawalAmount <= 0)
            throw new InvalidOperationAmountException
                    (Operation.Type.WITHDRAW.name(), withdrawalAmount, "Withdrawal amount cannot be 0 or less");
        int currentBalance = getBalance();
        if (withdrawalAmount > currentBalance)
            throw new InsufficientBalanceException(currentBalance);

        return history.addWithdrawal(withdrawalAmount);
    }

    /**
     * Get the current account balance (calculated from the history)
     *
     * @return The balance value
     */
    public int getBalance() {
        return history.calculateBalance();
    }

    /**
     * Get the history domain model to manage operations history
     *
     * @return The model
     */
    public History getHistory() {
        return history;
    }

    /**
     * Get account's number
     *
     * @return Account number
     */
    public int getNumber() {
        return number;
    }

    /**
     * Get account's owner name
     *
     * @return Owner name
     */
    public String getOwnerName() {
        return bankAccPersistencePort.getOwnerName(number);
    }

    /**
     * Get account's owner phone number
     *
     * @return Owner phone number
     */
    public String getOwnerPhoneNumber() {
        return bankAccPersistencePort.getOwnerPhoneNumber(number);
    }
}
