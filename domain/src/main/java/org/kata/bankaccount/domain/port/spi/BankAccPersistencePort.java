package org.kata.bankaccount.domain.port.spi;

import org.kata.bankaccount.domain.model.Operation;

import java.util.List;

/**
 * Bank account persistence port to implement
 */
public interface BankAccPersistencePort {
    /**
     * Load bank account operations
     *
     * @return List of operations
     */
    List<Operation> loadOperations();

    /**
     * Persist the operation details
     *
     * @param operation Operation to save
     * @return The saved operation
     */
    Operation saveOperation(Operation operation);

    /**
     * Creates a new bank account
     *
     * @param owner       Account owner's name
     * @param phoneNumber Account owner's phone number
     * @return New account's ID
     */
    int createNewAccount(String owner, String phoneNumber);

    /**
     * Get account owner's name
     *
     * @param accountNumber account number
     * @return Owner name
     */
    String getOwnerName(int accountNumber);

    /**
     * Get account owner's phone number
     *
     * @param accountNumber account number
     * @return Owner phone number
     */
    String getOwnerPhoneNumber(int accountNumber);
}
